package com.granados_raul_samso_joan.petitmon_v1.persistence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.Json;

import java.util.ArrayList;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Manage the persistence of the game
 */

public class PersistenceJson extends Json {

    private static ArrayList<String> arrayListPuntuaciones;
    private static Json json = new Json();
    private final static FileHandle fileHandle = Gdx.files.local("persistence/scores.json");

    public PersistenceJson(ArrayList<String> arrayListPuntuaciones) {
        this.arrayListPuntuaciones = arrayListPuntuaciones;
    }

    public static void writeArray(String[] arrayListPuntuaciones) {
        //fileHandle.writeString(Base64Coder.encodeString(arrayToJson(arrayListPuntuaciones)),false);
        fileHandle.writeString(Base64Coder.encodeString(json.prettyPrint(arrayListPuntuaciones)), false);
    }

    public static String[] readArray() {

        String[] arrayScores = new String[10];
        ArrayList scList=null;

        if (fileHandle !=null) {
            try{
                scList = json.fromJson(ArrayList.class, Base64Coder.decodeString(fileHandle.readString()));
            } catch (Exception e) {

            }
        }
        if ((fileHandle !=null)&&(scList!=null)) {
            for (int i=9;i>=0;i--) {
                //arrayScores[i]=String.valueOf((int)(Double.parseDouble(String.valueOf(arrayListPuntuaciones.get(i)))));
                arrayScores[i]= String.valueOf(scList.get(i));
            }
        } else {
            for (int i=9;i>=0;i--) {
                //arrayScores[i]=String.valueOf((int)(Double.parseDouble(String.valueOf(arrayListPuntuaciones.get(i)))));
                arrayScores[i]="0";
            }
            writeArray(arrayScores);
        }

        return arrayScores;
    }
}