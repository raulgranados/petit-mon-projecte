package com.granados_raul_samso_joan.petitmon_v1.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the main assets
 */

public class AssetsManager {

    public static Texture txtrBg, dinamicTxtrBg,
                          txtrFlagCa, txtrFlagEs,
                          txtrBack, txtrQuit,
                          txtrPlayerPrize,
                          txtrError;


    public static final void cargarAssets(){
        System.out.println("...\nLoading assets");
        txtrBg   = new Texture(Gdx.files.internal("backgrounds/background.png"));
        dinamicTxtrBg = new Texture(Gdx.files.internal("backgrounds/background_dinamic.png"));
        txtrFlagCa = new Texture(Gdx.files.internal("buttons/flag_ca.png"));
        txtrFlagEs = new Texture(Gdx.files.internal("buttons/flag_es.png"));
        txtrBack = new Texture(Gdx.files.internal("buttons/back.png"));
        txtrQuit = new Texture(Gdx.files.internal("buttons/quit.png"));
        txtrPlayerPrize = new Texture(Gdx.files.internal("players/player2.png"));
        txtrError   = new Texture(Gdx.files.internal("backgrounds/errorScreen.png"));
        System.out.println("End loading assets");
    }

    public static void disposeAssets(){
        txtrBg.dispose();
        dinamicTxtrBg.dispose();
        txtrFlagCa.dispose();
        txtrFlagEs.dispose();
        txtrBack.dispose();
        txtrQuit.dispose();
        txtrPlayerPrize.dispose();
    }


}
