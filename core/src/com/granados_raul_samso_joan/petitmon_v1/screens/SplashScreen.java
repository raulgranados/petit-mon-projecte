package com.granados_raul_samso_joan.petitmon_v1.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.granados_raul_samso_joan.petitmon_v1.animations.Bird;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the first screen
 */

public  class SplashScreen extends AbstractScreen {

    private GameMain game;

    private World world;

    private OrthographicCamera mainCamera;

    private Viewport gameViewport;

    private static OrthographicCamera box2DCamera;

    //private Box2DDebugRenderer debugRenderer;

    private Array<Sprite> backgrounds = new Array<Sprite>();

    private Texture textureTitle;

    private Bird blueBird;
    private Bird greenBird;

    public SplashScreen(GameMain game) {

        this.game = game;

        box2DCamera = new OrthographicCamera();
        box2DCamera.setToOrtho(false, GameInfo.WIDTH/ GameInfo.PPM, GameInfo.HEIGHT/ GameInfo.PPM);
        box2DCamera.position.set(GameInfo.WIDTH/2f, GameInfo.HEIGHT/2f, 0);


        mainCamera = new OrthographicCamera(GameInfo.WIDTH, GameInfo.HEIGHT);
        mainCamera.position.set(GameInfo.WIDTH / 2.0f, GameInfo.HEIGHT / 2.0f, 0);

        createBackgrounds();

        gameViewport = new StretchViewport(GameInfo.WIDTH, GameInfo.HEIGHT, mainCamera);

        //debugRenderer = new Box2DDebugRenderer();

        textureTitle = new Texture("backgrounds/titulo.png");

        world = new World(new Vector2(0, 0), true);

        blueBird = new Bird(world, "animationBird/birdblue.png" ,"animationBird/BirdBlueAnimation.atlas", GameInfo.WIDTH - 240f,  GameInfo.HEIGHT-325f);
        greenBird = new Bird(world,  "animationBird/birdgreen.png" ,"animationBird/BirdGreenAnimation.atlas", GameInfo.WIDTH - 440f, GameInfo.HEIGHT-25 );
        GameUtils.playSound(GameInfo.soundsRef[3]);

    }

    @Override
    public void buildStage(){}

    void createBackgrounds(){
        for (int i = 0; i < 3; i++) {
            Sprite bg = new Sprite(new Texture("backgrounds/background_dinamic.png"));
            bg.setPosition(i*bg.getWidth(), 0);
            backgrounds.add(bg);
        }
    }

    void drawBackgrounds(SpriteBatch batch){
        for (Sprite s : backgrounds) {
            batch.draw(s, s.getX(), s.getY());
        }
    }

    void moveBackgrounds() {
        for (Sprite bg : backgrounds) {
            float x1 = bg.getX() - 0.25f;
            bg.setPosition(x1, bg.getY());
            if (bg.getX() + bg.getWidth() + (bg.getWidth() / 2f) < mainCamera.position.x) {
                float x2 = bg.getX() + bg.getWidth() * backgrounds.size;
                bg.setPosition(x2, bg.getY());
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        for (Sprite s : backgrounds) {
            s.getTexture().dispose();
        }

        textureTitle.dispose();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        update(delta);

        //Clear screen
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mainCamera.update();
        game.getBatch().setProjectionMatrix(mainCamera.combined);

        game.getBatch().begin();

        drawBackgrounds( game.getBatch());

        blueBird.drawPlayerAnimation( game.getBatch());
        greenBird.drawPlayerAnimation( game.getBatch());
        game.getBatch().draw(textureTitle,(GameInfo.WIDTH-textureTitle.getWidth()) / 2.0f, (GameInfo.HEIGHT)- 330f);

        game.getBatch().end();

        //debugRenderer.render(world, box2DCamera.combined);

        //debugRenderer.render(world, mainCamera.combined);

        world.step(Gdx.graphics.getDeltaTime(), 6 , 2);
    }

    private void update(float delta) {
        moveBackgrounds();
    }









}
