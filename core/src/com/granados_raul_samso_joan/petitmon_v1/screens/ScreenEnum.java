package com.granados_raul_samso_joan.petitmon_v1.screens;

import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_one.GameOne;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_one.GameOneInstructions;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_one.GameOnePrize;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_one.GameOneScore;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_two.GameTwo;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_two.GameTwoInstructions;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_two.GameTwoPrize;
import com.granados_raul_samso_joan.petitmon_v1.screens.game_two.GameTwoScore;
import com.granados_raul_samso_joan.petitmon_v1.screens.menus.*;
import com.granados_raul_samso_joan.petitmon_v1.screens.menus.InstructionsMenu;
import com.granados_raul_samso_joan.petitmon_v1.screens.menus.ScoresMenu;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Classes list
 */

public enum ScreenEnum {
    SPLASH_SCREEN{
        public AbstractScreen getScreen(Object... params) {
            return new SplashScreen((GameMain) params[0]);
        }
    },
    LANGUAGE{
        public AbstractScreen getScreen(Object... params) {
            return new LanguagesMenu((GameMain) params[0]);
        }
    },
    MAIN_MENU {
        public AbstractScreen getScreen(Object... params) {
            return new MainMenu((GameMain) params[0]);
        }
    },
    SETTINGS {
        public AbstractScreen getScreen(Object... params) {
            return new SettingsMenu((GameMain) params[0]);
        }
    },
    INSTRUCTIONS {
        public AbstractScreen getScreen(Object... params) {
            return new InstructionsMenu((GameMain) params[0]);
        }
    },
    SCORES {
        public AbstractScreen getScreen(Object... params) {
            return new ScoresMenu((GameMain) params[0]);
        }
    },
    LEVELS {
        public AbstractScreen getScreen(Object... params) {
            return new LevelsMenu((GameMain) params[0]);
        }
    },

    GAMES {
        public AbstractScreen getScreen(Object... params) {
            return new GamesMenu((GameMain) params[0]) ;
        }
    },
    GAME_1 {
        public AbstractScreen getScreen(Object... params) {
            return new GameOne((GameMain) params[0]);
        }
    },
    GAME_1_SCORE {
        public AbstractScreen getScreen(Object... params) {
            return new GameOneScore((GameMain) params[0]);
        }
    },
    GAME_1_PRICE {
        public AbstractScreen getScreen(Object... params) {
            //yyyy
            //return new GameOnePrize_Old((GameMain) params[0]);
            //zzzz
            return new GameOnePrize((GameMain) params[0]);
        }
    },
    GAME_1_INSTRUCTIONS {
        public AbstractScreen getScreen(Object... params) {
            return new GameOneInstructions((GameMain) params[0]);
        }
    },
    GAME_2 {
        public AbstractScreen getScreen(Object... params) {
            return new GameTwo((GameMain) params[0]);
        }
    },
    GAME_2_SCORE {
        public AbstractScreen getScreen(Object... params) {
            return new GameTwoScore((GameMain) params[0]);
        }
    },
    GAME_2_PRICE {
        public AbstractScreen getScreen(Object... params) {
            //yyyy
            //return new GameTwoPrize_Old((GameMain) params[0]);
            return new GameTwoPrize((GameMain) params[0]);
        }
    },
    GAME_2_INSTRUCTIONS {
        public AbstractScreen getScreen(Object... params) {
            return new GameTwoInstructions((GameMain) params[0]);
        }
    },;

    public abstract AbstractScreen getScreen(Object... params);
}
