package com.granados_raul_samso_joan.petitmon_v1.screens.game_two;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the image textures of the cards in the game 2
 */

public class GameTwoCard extends Sprite {

    private World world;
    private Body body;

    public String name;

    public GameTwoCard(World world, float x, float y, String name){
        super(new Texture(name.substring(2)));
        this.world = world;
        setPosition(x, y);
        createBody(name);
    }

    void createBody(String name){

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set((getX()+43f)/ GameInfo.PPM, (getY())/GameInfo.PPM);

        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((((getWidth())/3f))/GameInfo.PPM, ((getHeight()/2.6f)/GameInfo.PPM)); //size of the box

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;

        Fixture fixture = body.createFixture(fixtureDef);

        fixture.setUserData(name);

        shape.dispose();
    }
}
