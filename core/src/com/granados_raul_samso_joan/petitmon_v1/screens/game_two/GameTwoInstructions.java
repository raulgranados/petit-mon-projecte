package com.granados_raul_samso_joan.petitmon_v1.screens.game_two;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.TimeUtils;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the instructions screen of the game 2
 */

public class GameTwoInstructions extends AbstractScreen {
    private GameMain game;

    private BitmapFont font1, font2,font3, font4;

    private long startTime = 0;

    public GameTwoInstructions(GameMain game) {
        this.game = game;
        startTime = TimeUtils.millis();
        createLabels();
    }

    @Override
    public void buildStage() {

        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        ScreenManager.getInstance().showScreen(ScreenEnum.INSTRUCTIONS, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });
    }

    void createLabels() {

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("fonts/blow.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                new FreeTypeFontGenerator.FreeTypeFontParameter();

        parameter.size = 50;

        font1 = generator.generateFont(parameter);
        font1.setColor(0f, 0f, 1.0f, 1.0f);
        font2 = generator.generateFont(parameter);
        font2.setColor(1f, 1f, 0f, 1.0f);
        font3 = generator.generateFont(parameter);
        font3.setColor(1f, 0f, 0f, 1.0f);
        font4 = generator.generateFont(parameter);
        font4.setColor(1f, 1f, 1f, 1.0f);
        //scoreLabel = new Label(String.valueOf(GameInfo.currentScore2),new Label.LabelStyle(font1, Color.WHITE));
        //scoreLabel2 = new Label(String.valueOf(GameInfo.currentScore2),new Label.LabelStyle(font1, Color.GREEN));
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        //Clear screen
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().begin();

        game.getBatch().draw(AssetsManager.txtrBg, 0.0f ,0.0f);

        font3.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][6], 50, GameInfo.HEIGHT/1.1f);
        font2.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][3]+" 1:\n\n"+ GameInfo.secondsLevelGame2[0]+" "+GameInfo.messageLang[GameInfo.langId][5]+",\n\n"+ GameInfo.levelLivesGame2[0]+" "+GameInfo.messageLang[GameInfo.langId][1], 170, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-110f);
        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][3]+" 2:\n\n"+ GameInfo.secondsLevelGame2[1]+" "+GameInfo.messageLang[GameInfo.langId][5]+",\n\n"+ GameInfo.levelLivesGame2[1]+" "+GameInfo.messageLang[GameInfo.langId][1], 170, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-310f);
        font4.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][3]+" 3:\n\n"+ GameInfo.secondsLevelGame2[2]+" "+GameInfo.messageLang[GameInfo.langId][5]+",\n\n"+ GameInfo.levelLivesGame2[2]+" "+GameInfo.messageLang[GameInfo.langId][1], 170, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-510f);


        game.getBatch().draw(AssetsManager.txtrBack, 10.0f, 10.0f);
        game.getBatch().draw(AssetsManager.txtrQuit, getWidth() - (AssetsManager.txtrQuit.getWidth() + 10.0f), 10.0f);

        game.getBatch().end();
    }

    @Override
    public void dispose() {
        super.dispose();

        font1.dispose();
        font2.dispose();
        font3.dispose();
        font4.dispose();
    }
}
