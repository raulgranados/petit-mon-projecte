package com.granados_raul_samso_joan.petitmon_v1.screens.game_two;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.granados_raul_samso_joan.petitmon_v1.animations.Cow;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the prize screen of the game 2
 */

public class GameTwoPrize extends AbstractScreen {
    private GameMain game;

    private World world;

    private Viewport gameViewport;

    private OrthographicCamera box2DCamera;
    private Box2DDebugRenderer debugRenderer;

    private long startTime = 0;

    //yyyy
    private Cow cowAnimation;

    Image prize;

    public GameTwoPrize(GameMain game) {

        this.game = game;

        box2DCamera = new OrthographicCamera();
        box2DCamera.setToOrtho(false, GameInfo.WIDTH/ GameInfo.PPM, GameInfo.HEIGHT/ GameInfo.PPM);
        box2DCamera.position.set(GameInfo.WIDTH/2f, GameInfo.HEIGHT/2f, 0);

        debugRenderer = new Box2DDebugRenderer();


        world = new World(new Vector2(0, 0), true);

        startTime = TimeUtils.millis();

        GameUtils.playMusic(GameInfo.musicRef[0]);

        cowAnimation = new Cow(world, "animationCow/cow_rotation2.png", GameInfo.WIDTH/3f, (4f/3f)* GameInfo.HEIGHT);
    }

    @Override
    public void buildStage() {
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        Image prize2 = new Image(new Texture(GameInfo.buttonsRef[16].concat(GameInfo.langRef[GameInfo.langId]).concat(".png")));
        prize2.setPosition(getWidth()*3f/4f , getHeight()*6f/8f , Align.center);
        addActor(prize2);

        GameUtils.playSound(GameInfo.soundsRef[0]);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        update(delta);;

        //VVVV
        //Clear screen
        //Gdx.gl.glClearColor(1, 1, 0, 1);
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().begin();

        cowAnimation.drawPlayerAnimation(game.getBatch());

        game.getBatch().end();

        //debugRenderer.render(world, box2DCamera.combined);
        //debugRenderer.render(world, mainCamera.combined);

        world.step(Gdx.graphics.getDeltaTime(), 6 , 2);
    }

    @Override
    public void dispose() {
        super.dispose();

        cowAnimation.getTexture().dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height, true);
    }

    @Override public void hide() {}

    @Override public void pause() {}

    @Override public void resume() {}


    void update(float dt) {
        checkTime(startTime);
    }

    public void checkTime(long startTime) {
        if (TimeUtils.timeSinceMillis(this.startTime) > 3 * 1000) { //3 second
            GameUtils.stopMusic();
            ScreenManager.getInstance().showScreen(ScreenEnum.GAME_2, game);
        }
    }
}
