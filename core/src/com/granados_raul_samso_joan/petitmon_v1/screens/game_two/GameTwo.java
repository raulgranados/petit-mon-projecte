package com.granados_raul_samso_joan.petitmon_v1.screens.game_two;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldManifold;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.persistence.PersistenceJson;
import com.granados_raul_samso_joan.petitmon_v1.player.Player;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the game 2
 */

public class GameTwo extends AbstractScreen implements ContactListener {

    private GameMain game;

    private World world;

    private Viewport gameViewport;

    private OrthographicCamera box2DCamera;

    private OrthographicCamera mainCamera;

    private Box2DDebugRenderer debugRenderer;

    private Array<Sprite> backgrounds = new Array<Sprite>();

    //Images cards
    private Sprite cardTest;
    private GameTwoCard card1, card2, card3, card4, card5, card6;
    private GameTwoCard cardA1, cardA2, cardA3, cardA4, cardA5, cardA6; //cards Back to guess
    private GameTwoCard cardB1, cardB2, cardB3, cardB4, cardB5, cardB6; //cards Back to guess

    private BitmapFont font1, font2,font3;

    private static int[] cardCollided;
    private static int cardCollidedId;
    private static String cardCollidedStringCard; //Name of the card
    private static String cardCollidedStrigNum;
    private static String cardsCollidedRef1;
    private static String cardsCollidedRef2;
    private static int cardsCollidedId1;
    private static int cardsCollidedId2;
    private static int cardsOpened;
    private static int turn;
    private long startTime;
    private long startTimeSecondCard;
    public static boolean currentScoreFoundInArray;
    private int numCardsBack; //Check maximum two cards back

    private Player player;

    int velX;
    int velY;

    public GameTwo(GameMain game) {
        this.game = game;

        velX=0;
        velY=0;
        numCardsBack=0;

        turn=0;
        cardsOpened=0;
        cardCollided= new int[]{0, 0, 0, 0, 0, 0, 0};
        currentScoreFoundInArray=false;


        box2DCamera = new OrthographicCamera();
        box2DCamera.setToOrtho(false, GameInfo.WIDTH/ GameInfo.PPM, GameInfo.HEIGHT/ GameInfo.PPM);
        box2DCamera.position.set(GameInfo.WIDTH/2f, GameInfo.HEIGHT/2f, 0);

        mainCamera = new OrthographicCamera(GameInfo.WIDTH, GameInfo.HEIGHT);
        mainCamera.position.set(GameInfo.WIDTH / 2.0f, GameInfo.HEIGHT / 2.0f, 0);

        createBackgrounds();

        gameViewport = new StretchViewport(GameInfo.WIDTH, GameInfo.HEIGHT, mainCamera);

        debugRenderer = new Box2DDebugRenderer();

        world = new World(new Vector2(0, 0), true);
        world.setContactListener(this);

        loadCards();

        startTime = TimeUtils.millis();
        startTimeSecondCard = 0;

        //Loop the music during (GameInfo.musicSeconds[0]) seconds
        GameUtils.playMusic(GameInfo.musicRef[1]);


        if (GameInfo.newGame2==true) {
            GameInfo.currentLife=GameInfo.levelLivesGame2[GameInfo.levelGame-1];
            GameInfo.currentScore2 =0;
            GameInfo.newGame2=false;
        }

        createLabels();

        player = new Player(world, (GameInfo.WIDTH/2f)+170, (GameInfo.HEIGHT/2f)+100f);
        player.pausePlayerY();
    }

    void createBackgrounds(){
        for (int i = 0; i < 3; i++) {
            Sprite bg = new Sprite(new Texture(Gdx.files.internal("backgrounds/background_dinamic.png")));
            bg.setPosition(i*bg.getWidth(), 0);
            backgrounds.add(bg);
        }
    }

    private void loadCards() {
        turn=0;

        cardsCollidedRef1="-";
        cardsCollidedRef2="-";
        cardsCollidedId1=99;
        cardsCollidedId2=99;

        //Card1 selection
        int idCard = GameUtils.getRandom(0, GameInfo.nCards);
        int idGroup = GameUtils.getRandom(0, GameInfo.nGrups);
        String[] group3Cards=new String[3];
        String newCard;
        newCard= "C".concat(GameUtils.getSufix(idGroup+1)).concat(GameUtils.getSufix(idCard+1)).concat(".png");
        group3Cards [0]=newCard;
        System.out.println("OK - "+newCard);

        //Card2 selection
        do {
            idCard = GameUtils.getRandom(0, GameInfo.nCards);
            idGroup = GameUtils.getRandom(0, GameInfo.nGrups);
            newCard="C".concat(GameUtils.getSufix(idGroup+1)).concat(GameUtils.getSufix(idCard+1)).concat(".png");
            System.out.println(newCard);
        } while (newCard.equals(group3Cards[0]));
        System.out.println("OK - "+newCard);
        group3Cards [1]=newCard;

        //Card3 selection
        do {
            idCard = GameUtils.getRandom(0, GameInfo.nCards);
            idGroup = GameUtils.getRandom(0, GameInfo.nGrups);
            newCard="C".concat(GameUtils.getSufix(idGroup+1)).concat(GameUtils.getSufix(idCard+1)).concat(".png");
            System.out.println(newCard);
        } while (newCard.equals(group3Cards[0])||newCard.equals(group3Cards[1]));
        System.out.println("OK - "+newCard);
        group3Cards[2]=newCard;
        for (int i=0;i<3;i++) {
            GameInfo.cRefListGame2.set(2*i,"cards/".concat(group3Cards[i]));
            GameInfo.cRefListGame2.set(2*i+1,"cards/".concat(group3Cards[i]));
            System.out.println("cards/".concat(group3Cards[i]));
        }

        Collections.shuffle(GameInfo.cRefListGame2);

        cardTest = createBody("X1".concat(GameInfo.cRefListGame2.get(0)));
        cardTest.setPosition(10000, 10000);

        float distanceVertical2=10f;
        float distanceFloor2=220f;
        float distanceMargin2=43f; //GameInfo.WIDTH/5f, //GameInfo.WIDTH/2.28f
        float distanceHoritzontal2=95f; //GameInfo.WIDTH/5f, //GameInfo.WIDTH/2.28f
        float distanceMargin3=GameInfo.WIDTH-2*distanceMargin2-cardTest.getWidth()+distanceHoritzontal2; //GameInfo.WIDTH/5f, //GameInfo.WIDTH/2.28f

        cardTest = new GameTwoCard(world, 10000, 10000, "X0".concat(GameInfo.cRefListGame2.get(0)));

        card1 = new GameTwoCard(world, distanceMargin2, (distanceFloor2+0*(cardTest.getHeight()+distanceVertical2)), "X1".concat(GameInfo.cRefListGame2.get(0)));
        card2 = new GameTwoCard(world, distanceMargin2, (distanceFloor2+1*(cardTest.getHeight()+distanceVertical2)), "X2".concat(GameInfo.cRefListGame2.get(1)));
        card3 = new GameTwoCard(world, distanceMargin2, (distanceFloor2+2*(cardTest.getHeight()+distanceVertical2)), "X3".concat(GameInfo.cRefListGame2.get(2)));
        card4 = new GameTwoCard(world, distanceMargin3, (distanceFloor2+0*(cardTest.getHeight()+distanceVertical2)), "X4".concat(GameInfo.cRefListGame2.get(3)));
        card5 = new GameTwoCard(world, distanceMargin3, (distanceFloor2+1*(cardTest.getHeight()+distanceVertical2)), "X5".concat(GameInfo.cRefListGame2.get(4)));
        card6 = new GameTwoCard(world, distanceMargin3, (distanceFloor2+2*(cardTest.getHeight()+distanceVertical2)), "X6".concat(GameInfo.cRefListGame2.get(5)));

        cardA1 = new GameTwoCard(world, 10000, (distanceFloor2+0*(cardTest.getHeight()+distanceVertical2)), "Y1cards/C0000.png");
        cardA2 = new GameTwoCard(world, 10000, (distanceFloor2+1*(cardTest.getHeight()+distanceVertical2)), "Y2cards/C0000.png");
        cardA3 = new GameTwoCard(world, 10000, (distanceFloor2+2*(cardTest.getHeight()+distanceVertical2)), "Y3cards/C0000.png");
        cardA4 = new GameTwoCard(world, 10000, (distanceFloor2+0*(cardTest.getHeight()+distanceVertical2)), "Y4cards/C0000.png");
        cardA5 = new GameTwoCard(world, 10000, (distanceFloor2+1*(cardTest.getHeight()+distanceVertical2)), "Y5cards/C0000.png");
        cardA6 = new GameTwoCard(world, 10000, (distanceFloor2+2*(cardTest.getHeight()+distanceVertical2)), "Y6cards/C0000.png");

        cardB1 = new GameTwoCard(world, 10000, (distanceFloor2+0*(cardTest.getHeight()+distanceVertical2)), "Z1cards/B0000.png");
        cardB2 = new GameTwoCard(world, 10000, (distanceFloor2+1*(cardTest.getHeight()+distanceVertical2)), "Z2cards/B0000.png");
        cardB3 = new GameTwoCard(world, 10000, (distanceFloor2+2*(cardTest.getHeight()+distanceVertical2)), "Z3cards/B0000.png");
        cardB4 = new GameTwoCard(world, 10000, (distanceFloor2+0*(cardTest.getHeight()+distanceVertical2)), "Z4cards/B0000.png");
        cardB5 = new GameTwoCard(world, 10000, (distanceFloor2+1*(cardTest.getHeight()+distanceVertical2)), "Z5cards/B0000.png");
        cardB6 = new GameTwoCard(world, 10000, (distanceFloor2+2*(cardTest.getHeight()+distanceVertical2)), "Z6cards/B0000.png");
    }

    public Sprite createBody(String name){
        Sprite sprite = new Sprite(new Texture(name.substring(2)));
        Body body;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set((sprite.getX()+43f)/GameInfo.PPM, (sprite.getY())/GameInfo.PPM);

        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((((sprite.getWidth())/3f))/GameInfo.PPM, ((sprite.getHeight()/2.6f)/GameInfo.PPM)); //size of the box

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;

        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(sprite.getTexture().getTextureData());

        shape.dispose();

        return sprite;
    }

    public void createLabels(){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("fonts/blow.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                new FreeTypeFontGenerator.FreeTypeFontParameter();

        parameter.size = 30;

        font1 = generator.generateFont(parameter);
        font1.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        font2 = generator.generateFont(parameter);
        font2.setColor(0f, 0, 1.0f, 1.0f);
        font3 = generator.generateFont(parameter);
        font3.setColor(1.0f, 0, 0, 1.0f);
    }

    @Override
    public void buildStage() {
        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameInfo.newGame2=true;
                        GameUtils.stopMusic();
                        ScreenManager.getInstance().showScreen(ScreenEnum.GAMES, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.stopMusic();
                        Gdx.app.exit();
                        return false;
                    }
                });
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        update(delta);

        player.updatePlayer();

        // Clear screen
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().begin();

        drawBackgrounds(game.getBatch());

        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][3]+GameInfo.typeLevel[GameInfo.langId][GameInfo.levelGame-1], 0, GameInfo.HEIGHT-(0.3f*(GameInfo.HEIGHT)/25f));
        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][2]+(GameInfo.secondsLevelGame2[GameInfo.levelGame-1]-(TimeUtils.timeSinceMillis(startTime)/1000)), GameInfo.WIDTH*2/3, GameInfo.HEIGHT-(0.3f*(GameInfo.HEIGHT)/25f));
        font2.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][0]+GameInfo.currentScore2, 0, GameInfo.HEIGHT-(1.5f*(GameInfo.HEIGHT)/25f));

        if(GameInfo.currentLife==1) {
            font3.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][1]+GameInfo.currentLife, GameInfo.WIDTH*2/3, GameInfo.HEIGHT-(1.5f*(GameInfo.HEIGHT)/25f));
        } else {
            font2.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][1]+GameInfo.currentLife, GameInfo.WIDTH*2/3, GameInfo.HEIGHT-(1.5f*(GameInfo.HEIGHT)/25f));
        }

        float distanceVertical=10f;
        float distanceFloor=120f;

        game.getBatch().draw(card1, 10f, distanceFloor);
        game.getBatch().draw(card2, 10f, distanceFloor+card1.getHeight()+1*distanceVertical);
        game.getBatch().draw(card3, 10f, distanceFloor+card1.getHeight()*2+distanceVertical);
        game.getBatch().draw(card4, GameInfo.WIDTH/1.5f, distanceFloor);
        game.getBatch().draw(card5, GameInfo.WIDTH/1.5f, distanceFloor+card1.getHeight()*1+distanceVertical);
        game.getBatch().draw(card6, GameInfo.WIDTH/1.5f, distanceFloor+card1.getHeight()*2+distanceVertical);

        //Card Blue already guessed
        if (cardCollided[0]==2) {game.getBatch().draw(cardB1, 10f, distanceFloor);}
        if (cardCollided[1]==2) {game.getBatch().draw(cardB2, 10f, distanceFloor+card1.getHeight()+1*distanceVertical);}
        if (cardCollided[2]==2) {game.getBatch().draw(cardB3, 10f, distanceFloor+card1.getHeight()*2+distanceVertical);}
        if (cardCollided[3]==2) {game.getBatch().draw(cardB4, GameInfo.WIDTH/1.5f, distanceFloor);}
        if (cardCollided[4]==2) {game.getBatch().draw(cardB5, GameInfo.WIDTH/1.5f, distanceFloor+card1.getHeight()*1+distanceVertical);}
        if (cardCollided[5]==2) {game.getBatch().draw(cardB6, GameInfo.WIDTH/1.5f, distanceFloor+card1.getHeight()*2+distanceVertical);}

        //Card Yellow to gues
        if (cardCollided[0]==0) {game.getBatch().draw(cardA1, 10f, distanceFloor);}
        if (cardCollided[1]==0) {game.getBatch().draw(cardA2, 10f, distanceFloor+card1.getHeight()+1*distanceVertical);}
        if (cardCollided[2]==0) {game.getBatch().draw(cardA3, 10f, distanceFloor+card1.getHeight()*2+distanceVertical);}
        if (cardCollided[3]==0) {game.getBatch().draw(cardA4, GameInfo.WIDTH/1.5f, distanceFloor);}
        if (cardCollided[4]==0) {game.getBatch().draw(cardA5, GameInfo.WIDTH/1.5f, distanceFloor+card1.getHeight()*1+distanceVertical);}
        if (cardCollided[5]==0) {game.getBatch().draw(cardA6, GameInfo.WIDTH/1.5f, distanceFloor+card1.getHeight()*2+distanceVertical);}

        game.getBatch().draw(AssetsManager.txtrBack, 10.0f, 10.0f);
        game.getBatch().draw(AssetsManager.txtrQuit, getWidth() - (AssetsManager.txtrQuit.getWidth() + 10.0f), 10.0f);

        player.drawPlayerIdle(game.getBatch());
        player.drawPlayerAnimetion(game.getBatch());

        game.getBatch().end();

        //debugRenderer.render(world, box2DCamera.combined);

        //debugRenderer.render(world, mainCamera.combined);

        game.getBatch().setProjectionMatrix(mainCamera.combined);
        mainCamera.update();

        world.step(Gdx.graphics.getDeltaTime(), 6 , 2);
    }

    @Override
    public void dispose() {

        for (Sprite s : backgrounds) {
            s.getTexture().dispose();
        }

        font1.dispose();
        font2.dispose();
        font3.dispose();

        card1.getTexture().dispose();
        card2.getTexture().dispose();
        card3.getTexture().dispose();
        card4.getTexture().dispose();
        card5.getTexture().dispose();
        card6.getTexture().dispose();

        cardA1.getTexture().dispose();
        cardA2.getTexture().dispose();
        cardA3.getTexture().dispose();
        cardA4.getTexture().dispose();
        cardA5.getTexture().dispose();
        cardA6.getTexture().dispose();

        cardB1.getTexture().dispose();
        cardB2.getTexture().dispose();
        cardB3.getTexture().dispose();
        cardB4.getTexture().dispose();
        cardB5.getTexture().dispose();
        cardB6.getTexture().dispose();

        player.getTexture().dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height, true);
    }

    @Override public void hide() {}

    @Override public void pause() {}

    @Override public void resume() {}

    void update(float dt) {
        moveBackgrounds();
        handleInput(dt);
        handleInputAndroid(dt);
        checkPlayerBounds(player);
        checkTime();
        checkTimeSecondCard();
    }

    void moveBackgrounds() {
        for (Sprite bg : backgrounds) {
            float x1 = bg.getX() - 0.25f;
            bg.setPosition(x1, bg.getY());
            if (bg.getX() + bg.getWidth() + (bg.getWidth() / 2f) < mainCamera.position.x) {
                float x2 = bg.getX() + bg.getWidth() * backgrounds.size;
                bg.setPosition(x2, bg.getY());
            }
        }
    }

    void handleInput(float dt) {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            player.setWalking(true);
            player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            player.setWalking(true);
            player.movePlayerX(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            player.setWalking(true);
            player.movePlayerY(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            player.setWalking(true);
            player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            player.setWalking(false);
            player.pausePlayerY();
        }
    }

    void handleInputAndroid(float dt) {
        if(Gdx.input.isTouched()) {
            player.setWalking(true);
            if((Gdx.input.getX() > (GameInfo.WIDTH / 2)) && (Gdx.input.getY()> (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            } else if((Gdx.input.getX() > (GameInfo.WIDTH / 2)) && (Gdx.input.getY()< (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            } else if((Gdx.input.getX() < (GameInfo.WIDTH / 2)) && (Gdx.input.getY()> (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            } else if((Gdx.input.getX() < (GameInfo.WIDTH / 2)) && (Gdx.input.getY()< (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            }
        } else if ((!Gdx.input.isKeyPressed(Input.Keys.LEFT))
                && (!Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                && (!Gdx.input.isKeyPressed(Input.Keys.UP))
                && (!Gdx.input.isKeyPressed(Input.Keys.DOWN))) {
            player.pausePlayerY();;
        }
    }

    public void checkPlayerBounds(Player player) {
        if(player.getY() - GameInfo.HEIGHT / 2f - player.getHeight() / 2f > box2DCamera.position.y - 115) { //Out of bounds up
            player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (player.getY() + GameInfo.HEIGHT / 2f + player.getHeight() / 2f < box2DCamera.position.y + 198){ //Out of bounds down
            player.movePlayerY(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        }
        if(player.getX() + 50 > GameInfo.WIDTH){ //Out of bounds right
              player.setWalking(false);
            player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (player.getX() + 410 < GameInfo.WIDTH) { //Out of bounds left
            player.movePlayerX(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        }
    }

    public void checkTime() {
        if (TimeUtils.timeSinceMillis(startTime) > GameInfo.secondsLevelGame2[GameInfo.levelGame - 1] * 1000) { //1 second
            GameUtils.playSound(GameInfo.soundsRef[11]);
            decreaseLives();
            startTime = TimeUtils.millis();
        }
    }

    public void decreaseLives() {
        GameInfo.currentLife--;
        if (GameInfo.currentLife<1) {
            endGame();
        }
    }

    public void endGame() {
        GameUtils.stopMusic();
        GameUtils.playSound(GameInfo.soundsRef[10]);
        GameInfo.newGame2=true;
        insertNewScore();
        ScreenManager.getInstance().showScreen(ScreenEnum.GAME_2_SCORE, game);
    }

    public void insertNewScore() {
        //Read Array
        GameInfo.arrayBestScores=PersistenceJson.readArray();
        GameInfo.arrayListIntegerbestScores=new ArrayList<Integer>();

        for(int ii=9;ii>=5;ii--) {
            GameInfo.arrayListIntegerbestScores.add(((int)(Double.parseDouble(String.valueOf(GameInfo.arrayBestScores[ii])))));
            if (GameInfo.currentScore2 ==((int)(Double.parseDouble(String.valueOf(GameInfo.arrayBestScores[ii]))))) {
                currentScoreFoundInArray=true;
            }
        }
        if (!currentScoreFoundInArray) {
            GameInfo.arrayListIntegerbestScores.add(GameInfo.currentScore2);
        }
        System.out.println("Abans ordenar: "+GameInfo.arrayListIntegerbestScores.toString());
        Comparator mycomparator = Collections.reverseOrder();
        Collections.sort(GameInfo.arrayListIntegerbestScores,mycomparator);
        System.out.println("Ordenat: "+GameInfo.arrayListIntegerbestScores.toString());
        for(int ii=0;ii<5;ii++) {
            GameInfo.arrayBestScores[ii+5]=String.valueOf(GameInfo.arrayListIntegerbestScores.get(ii));
        }
        PersistenceJson.writeArray(GameInfo.arrayBestScores);
    }


    public void checkTimeSecondCard() {
        if (turn>1&&turn%2==0&&TimeUtils.timeSinceMillis(startTimeSecondCard) > 2 * 1000) { //1 second
            checkSecondCard();
            resetCardsBack();
        }
    }

    public void checkSecondCard() {
        if (turn%2==0&&cardsCollidedId1!=99&&cardsCollidedId2!=99&&cardsCollidedRef1.equals(cardsCollidedRef2)) {
            //cardCollided >> Posició identificada amb 2 (ja no mostra carta)
            System.out.println("cardsCollidedId1: "+cardsCollidedId1);
            cardCollided[cardsCollidedId1] = 2;
            cardCollided[cardsCollidedId2] = 2;
            GameInfo.currentScore2 = GameInfo.currentScore2 + GameInfo.plusScore;
            cardsOpened=cardsOpened+2;

            //End the game when only rest the two equal cards
            if (cardsOpened==4) {
                cardCollided[0] = 2;
                cardCollided[1] = 2;
                cardCollided[2] = 2;
                cardCollided[3] = 2;
                cardCollided[4] = 2;
                cardCollided[5] = 2;

                //PEND
                startTime = TimeUtils.millis();
                checkTimeEndGame();
            }

            cardsCollidedRef1="-";
            cardsCollidedRef2="-";
            cardsCollidedId1=99;
            cardsCollidedId2=99;

        } else if (cardsCollidedId1!=99&&cardsCollidedId2!=99) {
            cardCollided[cardsCollidedId1] = 0;
            cardCollided[cardsCollidedId2] = 0;
        }
    }

    void resetCardsBack() {
        for (int i = 0; i < 6; i++) {
            if (cardCollided[i]==1) {
                cardCollided[i]=0;
            }
        }
        numCardsBack=0;
    }

    public void checkTimeEndGame() {
        if (TimeUtils.timeSinceMillis(startTimeSecondCard) > 2 * 1000) { //1 second
            GameUtils.stopMusic();
            ScreenManager.getInstance().showScreen(ScreenEnum.GAME_2_PRICE, game);
        }
    }

    void drawBackgrounds(SpriteBatch batch){
        for (Sprite s : backgrounds) {
            batch.draw(s, s.getX(), s.getY());
        }
    }

    @Override
    public void beginContact(Contact contact) {
        WorldManifold worldmanifold;
        worldmanifold = contact.getWorldManifold();

        for(Vector2 point : worldmanifold.getPoints()){
            System.out.println("Contact at : [" + point.x + ", " + point.y +"]");
        }

        System.out.println("Contacte2: "+ worldmanifold.getPoints()[0].x + "," + worldmanifold.getPoints()[0].y);

        cardCollidedStringCard =contact.getFixtureA().getUserData().toString();

        System.out.println("card: "+cardCollidedStringCard);

        cardCollidedStrigNum= cardCollidedStringCard.substring(1,2);
        cardCollidedId=Integer.parseInt(cardCollidedStrigNum)-1;
        if (numCardsBack<2&&cardCollided[cardCollidedId]==0) {
            cardCollided[cardCollidedId]=1;
            numCardsBack++;

            System.out.println("carta: "+cardCollidedStrigNum);
            System.out.println("cardCollidedId: "+cardCollidedId);

            if (cardCollidedId>=0&&cardCollidedId<=6) {
                turn++;
                if (turn % 2 == 0) {
                    cardsCollidedRef2 = cardCollidedStringCard.substring(2);
                    cardsCollidedId2 = cardCollidedId;
                    startTimeSecondCard = TimeUtils.millis();
                } else {
                    cardsCollidedRef1 = cardCollidedStringCard.substring(2);
                    cardsCollidedId1 = cardCollidedId;
                    startTimeSecondCard = 0;
                }
            } else {
                System.out.println("numCardsBack "+numCardsBack);
            }
        }
    }

    @Override
    public void endContact(Contact contact) {}

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {}

}