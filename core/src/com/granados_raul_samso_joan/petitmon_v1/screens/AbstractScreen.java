package com.granados_raul_samso_joan.petitmon_v1.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Establish the superclass for the screens
 */

public abstract class AbstractScreen extends Stage implements Screen {
    //private OrthographicCamera box2DCamer  >> Es podria implementar amb iguals propietats que la del fill, amb un objecte (o parametre)
    protected AbstractScreen() {
        super( new StretchViewport(GameInfo.WIDTH, GameInfo.HEIGHT, new OrthographicCamera()) ); // -> Invocación al constructor de la superclase Stage.
    }

    // buildStage() -> al tratarse de un método abstract, todas las subclases de AbstractScreen han de implementarlo.
    // La finalidad de este método es la de cargar los Actores
    public abstract void buildStage();

    @Override
    public void render(float delta) {
        // Clear screen
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Calling to Stage methods
        super.act(delta);
        super.draw();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height, true);
    }

    @Override public void hide() {}

    @Override public void pause() {}

    @Override public void resume() {}
}

