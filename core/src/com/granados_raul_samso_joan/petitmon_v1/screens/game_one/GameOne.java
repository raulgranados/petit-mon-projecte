package com.granados_raul_samso_joan.petitmon_v1.screens.game_one;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.persistence.PersistenceJson;
import com.granados_raul_samso_joan.petitmon_v1.player.Player;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the game 1
 */

public class GameOne extends AbstractScreen implements ContactListener  {

    private GameMain game;

    private World world;

    private Viewport gameViewport;

    private OrthographicCamera box2DCamera;

    private OrthographicCamera mainCamera;

    private Box2DDebugRenderer debugRenderer;

    private Array<Sprite> backgrounds = new Array<Sprite>();

    //Images cards
    private GameOneCard card1, card2, card3, card4;

    private int idGroup3Cards;
    private int idGroup1Card;
    private int idMismatchedCard;

    public static boolean currentScoreFoundInArray;

    private BitmapFont font1, font2,font3;

    private static long startTime = 0;

    private Player player;

    boolean error;
    private long startTimeErrorCard = 0;

    public GameOne(GameMain game) {
        this.game = game;

        error = false;

        box2DCamera = new OrthographicCamera();
        box2DCamera.setToOrtho(false, GameInfo.WIDTH/ GameInfo.PPM, GameInfo.HEIGHT/ GameInfo.PPM);
        box2DCamera.position.set(GameInfo.WIDTH/2f, GameInfo.HEIGHT/2f, 0);

        mainCamera = new OrthographicCamera(GameInfo.WIDTH, GameInfo.HEIGHT);
        mainCamera.position.set(GameInfo.WIDTH / 2.0f, GameInfo.HEIGHT / 2.0f, 0);

        createBackgrounds();

        gameViewport = new StretchViewport(GameInfo.WIDTH, GameInfo.HEIGHT, mainCamera);

        debugRenderer = new Box2DDebugRenderer();

        world = new World(new Vector2(0, 0), true);
        world.setContactListener(this);

        currentScoreFoundInArray=false;

        loadCards();

        startTime = TimeUtils.millis();

        //Loop the music during (GameInfo.musicSeconds[0]) seconds
        GameUtils.playMusic(GameInfo.musicRef[0]);

        if (GameInfo.newGame1 ==true) {
            GameInfo.currentLife=GameInfo.levelLivesGame1[GameInfo.levelGame-1];
            GameInfo.currentScore1 =0;
            GameInfo.newGame1 =false;
        }

        createLabels();

        player = new Player(world, GameInfo.WIDTH/2f, GameInfo.HEIGHT/2f + 100);
    }

    void createBackgrounds(){
        for (int i = 0; i < 3; i++) {
            Sprite bg = new Sprite(new Texture(Gdx.files.internal("backgrounds/background_dinamic.png")));
            bg.setPosition(i*bg.getWidth(), 0);
            backgrounds.add(bg);
        }
    }

    public void loadCards() {
        idMismatchedCard = GameUtils.getRandom(0, GameInfo.nCards);
        idGroup3Cards = GameUtils.getRandom(0, GameInfo.nGrups);

        do {
            idGroup1Card = GameUtils.getRandom(0, GameInfo.nGrups);
        } while (idGroup1Card - idGroup3Cards ==0);


        for(int i = 0; i< GameInfo.nCards; i++){
            GameInfo.cRefListGame1.set(i,"cards/".concat("C".concat(GameUtils.getSufix(idGroup3Cards +1)).concat(GameUtils.getSufix(i+1)).concat(".png")));
        }

        GameInfo.cRefListGame1.set(GameInfo.nCards,"cards/".concat("C".concat(GameUtils.getSufix(idGroup1Card +1)).concat(GameUtils.getSufix(idMismatchedCard +1)).concat(".png")));

        Collections.shuffle(GameInfo.cRefListGame1);

        float offsetX=(GameInfo.WIDTH/2.28f)+50f;
        card1 = new GameOneCard(world, offsetX, GameInfo.HEIGHT/1.14f, GameInfo.cRefListGame1.get(0));
        card2 = new GameOneCard(world, offsetX, GameInfo.HEIGHT/1.58f, GameInfo.cRefListGame1.get(1));
        card3 = new GameOneCard(world, offsetX, GameInfo.HEIGHT/2.51f, GameInfo.cRefListGame1.get(2));
        card4 = new GameOneCard(world, offsetX, GameInfo.HEIGHT/6.00f, GameInfo.cRefListGame1.get(3));
    }

    public void createLabels(){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("fonts/blow.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                new FreeTypeFontGenerator.FreeTypeFontParameter();

        parameter.size = 30;

        font1 = generator.generateFont(parameter);
        font1.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        font2 = generator.generateFont(parameter);
        font2.setColor(0f, 0, 1.0f, 1.0f);
        font3 = generator.generateFont(parameter);
        font3.setColor(1.0f, 0, 0, 1.0f);
    }

    @Override
    public void buildStage() {
        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameInfo.newGame1=true;
                        GameUtils.stopMusic();
                        ScreenManager.getInstance().showScreen(ScreenEnum.GAMES, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.stopMusic();
                        Gdx.app.exit();
                        return false;
                    }
                });

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        update(delta);

        player.updatePlayer();

        //Clear screen
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().begin();

        drawBackgrounds(game.getBatch());

        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][3]+"-"+GameInfo.typeLevel[GameInfo.langId][GameInfo.levelGame-1], 0, GameInfo.HEIGHT-(0.3f*(GameInfo.HEIGHT)/25f));
        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][2]+(GameInfo.secondsLevelGame1[GameInfo.levelGame-1]-(TimeUtils.timeSinceMillis(startTime)/1000)), 0, GameInfo.HEIGHT-(1.5f*(GameInfo.HEIGHT)/25f));
        font2.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][0]+GameInfo.currentScore1, 0, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f)));

        if(GameInfo.currentLife==1) {
            font3.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][1]+GameInfo.currentLife, 0, GameInfo.HEIGHT-(4.5f*(GameInfo.HEIGHT)/25f));
        } else {
            font2.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][1]+GameInfo.currentLife, 0, GameInfo.HEIGHT-(4.5f*(GameInfo.HEIGHT)/25f));
        }

        game.getBatch().draw(card1, GameInfo.WIDTH/2.10f, GameInfo.HEIGHT/1.33f);
        game.getBatch().draw(card2, GameInfo.WIDTH/2.10f, GameInfo.HEIGHT/1.95f);
        game.getBatch().draw(card3, GameInfo.WIDTH/2.10f, GameInfo.HEIGHT/3.65f);
        game.getBatch().draw(card4, GameInfo.WIDTH/2.10f, GameInfo.HEIGHT/24.50f);


        if (error) {
           game.getBatch().draw(AssetsManager.txtrError, -150f, (GameInfo.HEIGHT-200)/2f);
        }
        game.getBatch().draw(AssetsManager.txtrBack, 10.0f, 10.0f);
        game.getBatch().draw(AssetsManager.txtrQuit, getWidth() - (AssetsManager.txtrQuit.getWidth() + 10.0f), 10.0f);


        game.getBatch().draw(player, player.getX()-70f, player.getY()-120f);


        game.getBatch().end();

        //debugRenderer.render(world, box2DCamera.combined);

        //debugRenderer.render(world, mainCamera.combined);

        game.getBatch().setProjectionMatrix(mainCamera.combined);
        mainCamera.update();

        world.step(Gdx.graphics.getDeltaTime(), 6 , 2);
    }

    @Override
    public void dispose() {
        super.dispose();

        for (Sprite s : backgrounds) {
            s.getTexture().dispose();
        }

        font1.dispose();
        font2.dispose();
        font3.dispose();

        card1.getTexture().dispose();
        card2.getTexture().dispose();
        card3.getTexture().dispose();
        card4.getTexture().dispose();

        player.getTexture().dispose();
    }

    @Override public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override public void resize(int width, int height) { getViewport().update(width, height, true); }

    @Override public void hide() {}

    @Override public void pause() {}

    @Override public void resume() {}

    void update(float dt) {
        moveBackgrounds();
        handleInput(dt);
        handleInputAndroid(dt);
        checkTimeErrorCard();
        checkPlayerBounds(player);
        checkTime();
    }

    void moveBackgrounds() {
        for (Sprite bg : backgrounds) {
            float x1 = bg.getX() - 0.25f;
            bg.setPosition(x1, bg.getY());
            if (bg.getX() + bg.getWidth() + (bg.getWidth() / 2f) < mainCamera.position.x) {
                float x2 = bg.getX() + bg.getWidth() * backgrounds.size;
                bg.setPosition(x2, bg.getY());
            }
        }
    }

    void handleInput(float dt) {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            player.movePlayerX(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            player.movePlayerY(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            player.pausePlayerY();
        }
    }

    void handleInputAndroid(float dt) {
        if(Gdx.input.isTouched()) {
            if((Gdx.input.getX() > (GameInfo.WIDTH / 2)) && (Gdx.input.getY()> (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            } else if((Gdx.input.getX() > (GameInfo.WIDTH / 2)) && (Gdx.input.getY()< (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            } else if((Gdx.input.getX() < (GameInfo.WIDTH / 2)) && (Gdx.input.getY()> (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            } else if((Gdx.input.getX() < (GameInfo.WIDTH / 2)) && (Gdx.input.getY()< (GameInfo.HEIGHT / 2))) {
                player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
                player.movePlayerY(+GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
            }
        } else if ((!Gdx.input.isKeyPressed(Input.Keys.LEFT))
                        && (!Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                        && (!Gdx.input.isKeyPressed(Input.Keys.UP))
                        && (!Gdx.input.isKeyPressed(Input.Keys.DOWN))) {
            player.pausePlayerY();
        }
    }

    public void checkTimeErrorCard() {
        if (TimeUtils.timeSinceMillis(startTimeErrorCard) > 2 * 1000) { //1 second
            error =false;
            startTimeErrorCard = 0;
        }
    }

    public void checkPlayerBounds(Player player) {
        if(player.getY() - GameInfo.HEIGHT / 2f - player.getHeight() / 2f > box2DCamera.position.y - 115) { //Out of bounds up
            player.movePlayerY(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (player.getY() + GameInfo.HEIGHT / 2f + player.getHeight() / 2f < box2DCamera.position.y + 198){ //Out of bounds down
            player.movePlayerY(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        }
        if(player.getX() + 50 > GameInfo.WIDTH){ //Out of bounds right
            player.movePlayerX(-GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        } else if (player.getX() + 410 < GameInfo.WIDTH) { //Out of bounds left
            player.movePlayerX(GameInfo.PLAYERVELOCITIES[GameInfo.levelGame-1]);
        }
    }

    public void checkTime() {
        if (TimeUtils.timeSinceMillis(startTime) > GameInfo.secondsLevelGame1[GameInfo.levelGame - 1] * 1000) { //1 second
            GameUtils.playSound(GameInfo.soundsRef[11]);
            startTime = TimeUtils.millis();
            decreaseLives();
        }
    }

    public void decreaseLives() {
        GameInfo.currentLife--;
        if (GameInfo.currentLife<1) {
            endGame();
        }
    }

    public void endGame() {
        //Se supone que antes se ha llamado a GameUtils.playMusic(GameInfo.musicRef[0]);

        GameUtils.stopMusic();
        GameUtils.playSound(GameInfo.soundsRef[10]);

        GameInfo.newGame1 =true;

        insertNewScore();

        ScreenManager.getInstance().showScreen(ScreenEnum.GAME_1_SCORE, game);
    }

    public void insertNewScore() {

        //Read the Array
        GameInfo.arrayBestScores=PersistenceJson.readArray();
        GameInfo.arrayListIntegerbestScores=new ArrayList<Integer>();

        //Check if the currentScore is in the 5 top scores
        for(int ii=4;ii>=0;ii--) {
            GameInfo.arrayListIntegerbestScores.add(((int)(Double.parseDouble(String.valueOf(GameInfo.arrayBestScores[ii])))));
            if (GameInfo.currentScore1 ==((int)(Double.parseDouble(String.valueOf(GameInfo.arrayBestScores[ii]))))) {
                currentScoreFoundInArray=true;
            }
        }

        //Insert the new currentScore (only if it's not in the 5 top scores)
        if (!currentScoreFoundInArray) {
            GameInfo.arrayListIntegerbestScores.add(GameInfo.currentScore1);
        }

        System.out.println("Before sorted: "+GameInfo.arrayListIntegerbestScores.toString());
        //Sort the arrayList
        Comparator mycomparator = Collections.reverseOrder();
        Collections.sort(GameInfo.arrayListIntegerbestScores,mycomparator);
        System.out.println("Once sorted: "+GameInfo.arrayListIntegerbestScores.toString());

        //Insert the 5 top arrayList values of the Game 1 to array
        for(int ii=0;ii<5;ii++) {
            GameInfo.arrayBestScores[ii]=String.valueOf(GameInfo.arrayListIntegerbestScores.get(ii));
        }

        //write all the scores in to the file
        PersistenceJson.writeArray(GameInfo.arrayBestScores);
    }

    void drawBackgrounds(SpriteBatch batch){
        for (Sprite s : backgrounds) {
            batch.draw(s, s.getX(), s.getY());
        }
    }

    @Override
    public void beginContact(Contact contact) {
        int idGroup1CardInt;
        idGroup1CardInt = (idGroup1Card)+1;

        System.out.println("Contacte amb: "+contact.getFixtureA().getUserData().toString());
        String dataContact=contact.getFixtureA().getUserData().toString();

        if (Integer.parseInt(dataContact.substring(7,9))-idGroup1CardInt==0) {
            //GameUtils.playSound(GameInfo.soundsRef[1]);
            GameInfo.currentScore1 =GameInfo.currentScore1 +GameInfo.plusScore;
            GameUtils.stopMusic(); //Falta parámetro música
            ScreenManager.getInstance().showScreen(ScreenEnum.GAME_1_PRICE, game);
            //ScreenManager.getInstance().showScreen(ScreenEnum.GAME_1_SCORE, game);
            System.out.println("Well done: "+dataContact.substring(7,9)+"-"+idGroup1CardInt);
        } else {
            GameUtils.playSound(GameInfo.soundsRef[9]);
            error = true;
            startTimeErrorCard = TimeUtils.millis();
            decreaseLives();
            System.out.println(Integer.parseInt(dataContact.substring(7,9))+"-"+idGroup1CardInt);
        }
    }

    @Override
    public void endContact(Contact contact) {}

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {}






}
