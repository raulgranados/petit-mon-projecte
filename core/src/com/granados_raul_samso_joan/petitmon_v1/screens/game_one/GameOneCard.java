package com.granados_raul_samso_joan.petitmon_v1.screens.game_one;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the image textures of the cards in the game 1
 */

public class GameOneCard extends Sprite {

    private World world;
    private Body body;

    public String name;

    public GameOneCard(World world, float x, float y, String name){
        //CJS5
        //super(new Texture(name));
        super(new Texture(name));
        this.world = world;
        setPosition(x, y);
        //CJS5
        //createBody(name);
        createBody(name);
    }

    void createBody(String name){

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        //bodyDef.position.set((getX()+24f)/GameInfo.PPM, (getY()+132f)/GameInfo.PPM);
        bodyDef.position.set((getX()+43f)/ GameInfo.PPM, (getY())/GameInfo.PPM);

        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        //shape.setAsBox((((getWidth())/4.3f))/GameInfo.PPM, ((getHeight()/3.6f)/GameInfo.PPM)); //size of the box
        shape.setAsBox((((getWidth())/3f))/GameInfo.PPM, ((getHeight()/2.6f)/GameInfo.PPM)); //size of the box

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.isSensor();
        fixture.setUserData(name);

        shape.dispose();
    }
}
