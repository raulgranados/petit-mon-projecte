package com.granados_raul_samso_joan.petitmon_v1.screens.game_one;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.TimeUtils;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.persistence.PersistenceJson;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the scores screen of the game 1
 */

public class GameOneScore extends AbstractScreen {

    private GameMain game;

    private long startTime = 0;

    private BitmapFont font1, font2,font3;

    public GameOneScore(GameMain game) {
        this.game = game;

        startTime = TimeUtils.millis();

        //GameUtils.playMusic(GameInfo.musicRef[0]);

        GameInfo.arrayBestScores= PersistenceJson.readArray();

        createLabels();
    }

    void createLabels() {

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("fonts/blow.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                new FreeTypeFontGenerator.FreeTypeFontParameter();

        parameter.size = 50;

        font1 = generator.generateFont(parameter);
        font2 = generator.generateFont(parameter);
        font3 = generator.generateFont(parameter);

    }

    @Override
    public void buildStage() {
        // Adding actors
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        if (!GameInfo.varScoresMenu) {
                            ScreenManager.getInstance().showScreen(ScreenEnum.GAMES, game);
                        } else {
                            ScreenManager.getInstance().showScreen(ScreenEnum.SCORES, game);
                        }
                        GameInfo.varScoresMenu=false;
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        checkTime();

        //Clear screen
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().begin();

        game.getBatch().draw(AssetsManager.txtrBg, 0.0f ,0.0f);

        font1.setColor(0f, 0f, 1.0f, 1.0f);
        font2.setColor(1f, 0f, 0f, 1.0f);
        font3.setColor(1f, 1f, 1f, 1.0f);

        font3.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][8]+" 1", 145, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))+50f);
        font2.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][9]+": "+ GameInfo.currentScore1, 145, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-70f);

        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][10]+"1: "+GameInfo.arrayBestScores[0], 145, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-140f);
        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][10]+"2: "+GameInfo.arrayBestScores[1], 145, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-240f);
        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][10]+"3: "+GameInfo.arrayBestScores[2], 145, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-340f);
        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][10]+"4: "+GameInfo.arrayBestScores[3], 145, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-440f);
        font1.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][10]+"5: "+GameInfo.arrayBestScores[4], 145, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))-540f);


        game.getBatch().draw(AssetsManager.txtrBack, 10.0f, 10.0f);
        game.getBatch().draw(AssetsManager.txtrQuit, getWidth() - (AssetsManager.txtrQuit.getWidth() + 10.0f), 10.0f);

        game.getBatch().end();
    }

    public void checkTime() {
        if (TimeUtils.timeSinceMillis(startTime) > 5 * 1000) { //1 second
            GameInfo.currentScore1 =0;
            if (GameInfo.newGame1) {
                ScreenManager.getInstance().showScreen(ScreenEnum.GAMES, game);
            } else {
                ScreenManager.getInstance().showScreen(ScreenEnum.SCORES, game);
            }
        }
    }
}