package com.granados_raul_samso_joan.petitmon_v1.screens.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the language preferences menu
 */

public class LanguagesMenu extends AbstractScreen implements Screen {

    private GameMain game;

    public LanguagesMenu(GameMain game) {
        this.game = game;
    }

    @Override
    public void buildStage() {
        // Adding actors
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        ImageButton flagCa = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrFlagCa)));
        flagCa.setPosition((getWidth() + flagCa.getWidth()/2 )/2, 500.0f, Align.center);
        addActor(flagCa);

        flagCa.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameInfo.langId=1;
                        GameUtils.playSound(GameInfo.soundsRef[0]);
                        ScreenManager.getInstance().showScreen( ScreenEnum.MAIN_MENU, game);
                        return false;
                    }
                });

        ImageButton flagEs = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrFlagEs)));
        flagEs.setPosition((getWidth() + flagCa.getWidth()/2)/2, 300.0f, Align.center);
        addActor(flagEs);

        flagEs.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameInfo.langId=0;
                        GameUtils.playSound(GameInfo.soundsRef[0]);
                        ScreenManager.getInstance().showScreen( ScreenEnum.MAIN_MENU, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });
    }


    @Override
    public void dispose() {
        super.dispose();
    }
}
