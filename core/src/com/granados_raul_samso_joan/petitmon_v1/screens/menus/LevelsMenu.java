package com.granados_raul_samso_joan.petitmon_v1.screens.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the levels menu
 */

public class LevelsMenu extends AbstractScreen {

    private GameMain game;

    private Texture txtrLow, txtrMedium, txtrHigh;
    private ImageButton low, medium, high;

    public LevelsMenu(GameMain game) {
        this.game = game;
    }

    @Override
    public void buildStage() {
        // Adding actors
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        txtrLow = new Texture(GameInfo.buttonsRef[9].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        txtrMedium = new Texture(GameInfo.buttonsRef[10].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        txtrHigh = new Texture(GameInfo.buttonsRef[11].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        low = new ImageButton(new SpriteDrawable(new Sprite(txtrLow)));
        low.setPosition(getWidth()/2, 700.0f, Align.center);
        addActor(low);
        low.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[8]);
                        GameInfo.levelGame =1;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        return false;
                    }
                });
        medium = new ImageButton(new SpriteDrawable(new Sprite(txtrMedium)));
        medium.setPosition(getWidth()/2, 600.0f, Align.center);
        addActor(medium);
        medium.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[8]);
                        GameInfo.levelGame =2;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        return false;
                    }
                });
        high = new ImageButton(new SpriteDrawable(new Sprite(txtrHigh)));
        high.setPosition(getWidth()/2, 500.0f, Align.center);
        addActor(high);
        high.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[8]);
                        GameInfo.levelGame =3;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        return false;
                    }
                });


        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });



    }
}
