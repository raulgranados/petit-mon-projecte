package com.granados_raul_samso_joan.petitmon_v1.screens.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the main menu
 */

public class MainMenu extends AbstractScreen {

    private GameMain game;

    private Texture txtrGames, txtrSettings, txtrInstrucctions, txtrScores;

    public MainMenu(GameMain game) {
        this.game = game;
    }

    @Override
    public void buildStage() {
        // Adding actors
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        txtrGames = new Texture(GameInfo.buttonsRef[1].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        ImageButton gameBtn = new ImageButton(new SpriteDrawable(new Sprite(txtrGames)));
        gameBtn.setPosition(getWidth()/2, 700.0f, Align.center);
        addActor(gameBtn);

        gameBtn.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[1]);
                        ScreenManager.getInstance().showScreen(ScreenEnum.GAMES, game);
                        return false;
                    }
                });

        txtrSettings = new Texture(GameInfo.buttonsRef[2].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        ImageButton settingsBtn = new ImageButton(new SpriteDrawable(new Sprite(txtrSettings)));
        settingsBtn.setPosition(getWidth()/2, 550.0f, Align.center);
        addActor(settingsBtn);

        settingsBtn.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[1]);
                        ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        return false;
                    }
                });

        txtrInstrucctions = new Texture(GameInfo.buttonsRef[13].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        ImageButton instructionsBtn = new ImageButton(new SpriteDrawable(new Sprite(txtrInstrucctions)));
        instructionsBtn.setPosition(getWidth()/2, 400.0f, Align.center);
        addActor(instructionsBtn);

        instructionsBtn.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[1]);
                        ScreenManager.getInstance().showScreen(ScreenEnum.INSTRUCTIONS, game);
                        return false;
                    }
                });

        txtrScores = new Texture(GameInfo.buttonsRef[12].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        ImageButton scoresBtn = new ImageButton(new SpriteDrawable(new Sprite(txtrScores)));
        scoresBtn.setPosition(getWidth()/2, 250.0f, Align.center);
        addActor(scoresBtn);

        scoresBtn.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[1]);
                        ScreenManager.getInstance().showScreen(ScreenEnum.SCORES, game);
                        return false;
                    }
                });

        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        ScreenManager.getInstance().showScreen(ScreenEnum.LANGUAGE, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });
    }

    @Override
    public void dispose() {
        super.dispose();
        txtrGames.dispose();
        txtrSettings.dispose();
        txtrInstrucctions.dispose();
        txtrScores.dispose();
    }
}
