package com.granados_raul_samso_joan.petitmon_v1.screens.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the settings menu
 */

public class SettingsMenu extends AbstractScreen {

    private GameMain game;
    private Texture txtrMusicaYes, txtrMusicaNo, txtrSonidosYes, txtrSonidosNo, txtrNiveles ;
    //yyyy
    //music >> musics
    private ImageButton musics, sounds, levels;

    public SettingsMenu(GameMain game) {
        this.game = game;
    }

    @Override
    public void buildStage() {

        // Adding actors
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        txtrMusicaYes = new Texture(GameInfo.buttonsRef[14].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        txtrMusicaNo = new Texture(GameInfo.buttonsRef[15].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        txtrSonidosYes = new Texture(GameInfo.buttonsRef[5].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        txtrSonidosNo = new Texture(GameInfo.buttonsRef[6].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        txtrNiveles = new Texture(GameInfo.buttonsRef[7].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));

        if (GameInfo.musicActivate) {
            musics = new ImageButton(new SpriteDrawable(new Sprite(txtrMusicaYes)));
        } else {
            musics = new ImageButton(new SpriteDrawable(new Sprite(txtrMusicaNo)));
            //yyyy
            //GameUtils.stopMusic(GameInfo.soundsRef[1]);
            //GameUtils.stopMusic();
        }
        musics.setPosition(getWidth()/2, 700.0f, Align.center);
        addActor(musics);

        musics.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        //yyyy
                        GameInfo.musicActivate=!GameInfo.musicActivate;
                        ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        GameUtils.playSound(GameInfo.soundsRef[5]);

                        /*)
                        if(GameInfo.musicActivate == true) {
                            GameInfo.musicActivate = false;
                            //yyyy
                            //GameUtils.stopMusic(GameInfo.soundsRef[1]);
                            ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        } else {
                            GameInfo.musicActivate = true;
                            ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        }
                        */
                        return false;
                    }
                });

        //yyyy
        if (GameInfo.soundsActivate){
            sounds = new ImageButton(new SpriteDrawable(new Sprite(txtrSonidosYes)));
        } else {
            sounds = new ImageButton(new SpriteDrawable(new Sprite(txtrSonidosNo)));
        }

        sounds.setPosition(getWidth()/2, 600.0f, Align.center);
        addActor(sounds);

        sounds.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        //yyyy
                        GameInfo.soundsActivate=!GameInfo.soundsActivate;
                        ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        GameUtils.playSound(GameInfo.soundsRef[5]);
                        /*
                        if(GameInfo.soundsActivate == true) {
                            GameInfo.soundsActivate = false;
                            ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        } else {
                            GameInfo.soundsActivate = true;
                            ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS, game);
                        }
                        */
                        return false;
                    }
                });

        levels = new ImageButton(new SpriteDrawable(new Sprite(txtrNiveles)));
        levels.setPosition(getWidth()/2, 500.0f, Align.center);
        addActor(levels);

        levels.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        ScreenManager.getInstance().showScreen(ScreenEnum.LEVELS, game);
                        //yyyy
                        GameUtils.playSound(GameInfo.soundsRef[5]);
                        return false;
                    }
                });

        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        ScreenManager.getInstance().showScreen(ScreenEnum.MAIN_MENU, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });

    }
}
