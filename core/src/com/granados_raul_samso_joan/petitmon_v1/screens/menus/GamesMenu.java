package com.granados_raul_samso_joan.petitmon_v1.screens.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the games menu
 */

public class GamesMenu extends AbstractScreen {

    private GameMain game;

    Texture txtrGame1, txtrGame2;

    public GamesMenu(GameMain game) {
        this.game = game;
    }

    @Override
    public void buildStage() {
        // Adding actors
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        txtrGame1 = new Texture(GameInfo.buttonsRef[3].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        ImageButton game1 = new ImageButton(new SpriteDrawable(new Sprite(txtrGame1)));
        game1.setPosition(getWidth()/2, 700.0f, Align.center);
        addActor(game1);
        game1.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[3]);
                        System.out.println("GameOne is touched");
                        ScreenManager.getInstance().showScreen(ScreenEnum.GAME_1, game );
                        return false;
                    }
                });

        txtrGame2 = new Texture(GameInfo.buttonsRef[4].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        ImageButton game2 = new ImageButton(new SpriteDrawable(new Sprite(txtrGame2)));
        game2.setPosition(getWidth()/2, 500.0f, Align.center);
        addActor(game2);

        game2.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[3]);
                        System.out.println("GameTwo is touched");
                        ScreenManager.getInstance().showScreen(ScreenEnum.GAME_2, game);
                        return false;
                    }
                });



        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        ScreenManager.getInstance().showScreen(ScreenEnum.MAIN_MENU, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });
    }

    @Override
    public void dispose() {
        super.dispose();
        txtrGame1.dispose();
        txtrGame2.dispose();
    }
}
