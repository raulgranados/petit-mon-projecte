package com.granados_raul_samso_joan.petitmon_v1.screens.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameUtils;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.AbstractScreen;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Load the scores menu
 */

public class ScoresMenu extends AbstractScreen {
    private GameMain game;

    private BitmapFont font1, font2,font3;

    private Texture txtrGame1, txtrGame2;

    public ScoresMenu(GameMain game) {
        this.game = game;
        txtrGame1 = new Texture(GameInfo.buttonsRef[3].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        txtrGame2 = new Texture(GameInfo.buttonsRef[4].concat(GameInfo.langRef[GameInfo.langId]).concat(".png"));
        createLabels();
    }

    @Override
    public void buildStage() {
        // Adding actors
        Image bg = new Image(AssetsManager.txtrBg);
        addActor(bg);

        ImageButton game1 = new ImageButton(new SpriteDrawable(new Sprite(txtrGame1)));
        game1.setPosition((getWidth()-(txtrGame1.getWidth()))/2, 600.0f);
        addActor(game1);

        game1.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[2]);
                        //yyyy
                        GameInfo.varScoresMenu=true;
                        ScreenManager.getInstance().showScreen(ScreenEnum.GAME_1_SCORE, game );
                        return false;
                    }
                });

        ImageButton game2 = new ImageButton(new SpriteDrawable(new Sprite(txtrGame2)));
        game2.setPosition((getWidth()-(txtrGame2.getWidth()))/2, 500.0f);
        addActor(game2);

        game2.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        GameUtils.playSound(GameInfo.soundsRef[2]);
                        //yyyy
                        GameInfo.varScoresMenu=true;
                        ScreenManager.getInstance().showScreen(ScreenEnum.GAME_2_SCORE, game);
                        return false;
                    }
                });

        ImageButton back = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrBack)));
        back.setPosition(10.0f, 10.0f);
        addActor(back);

        back.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        ScreenManager.getInstance().showScreen(ScreenEnum.MAIN_MENU, game);
                        return false;
                    }
                });

        ImageButton quit = new ImageButton(new SpriteDrawable(new Sprite(AssetsManager.txtrQuit)));
        quit.setPosition(getWidth() - (quit.getWidth() + 10.0f), 10.0f);
        addActor(quit);

        quit.addListener(
                new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event,
                                             float x, float y, int pointer, int button) {
                        Gdx.app.exit();
                        return false;
                    }
                });
    }

    void createLabels() {

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("fonts/blow.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                new FreeTypeFontGenerator.FreeTypeFontParameter();

        parameter.size = 50;

        font1 = generator.generateFont(parameter);
        font1.setColor(0f, 0f, 1.0f, 1.0f);
        font2 = generator.generateFont(parameter);
        font2.setColor(1f, 0f, 0f, 1.0f);
        font3 = generator.generateFont(parameter);
        font3.setColor(1f, 1f, 1f, 1.0f);
        //scoreLabel = new Label(String.valueOf(GameInfo.currentScore1),new Label.LabelStyle(font1, Color.WHITE));
        //scoreLabel2 = new Label(String.valueOf(GameInfo.currentScore1),new Label.LabelStyle(font1, Color.GREEN));

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        //Clear screen
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().begin();

        game.getBatch().draw(AssetsManager.txtrBg, 0.0f ,0.0f);

        font2.draw(game.getBatch(), GameInfo.messageLang[GameInfo.langId][12], 50, GameInfo.HEIGHT-(3f*(GameInfo.HEIGHT/25f))+60f);

        game.getBatch().draw(new Texture(GameInfo.buttonsRef[3].concat(GameInfo.langRef[GameInfo.langId]).concat(".png")), (getWidth()-(txtrGame1.getWidth()))/2, 600.0f);
        game.getBatch().draw(new Texture(GameInfo.buttonsRef[4].concat(GameInfo.langRef[GameInfo.langId]).concat(".png")), (getWidth()-(txtrGame2.getWidth()))/2, 500.0f);

        game.getBatch().draw(AssetsManager.txtrBack, 10.0f, 10.0f);
        game.getBatch().draw(AssetsManager.txtrQuit, getWidth() - (AssetsManager.txtrQuit.getWidth() + 10.0f), 10.0f);


        game.getBatch().end();

    }
}
