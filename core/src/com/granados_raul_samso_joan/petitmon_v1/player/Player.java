package com.granados_raul_samso_joan.petitmon_v1.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Manage the player of the games
 */

public class Player extends Sprite {

    private World world;
    private Body body;

    private TextureAtlas palyerAtlas;

    private float elapsedTime;

    private boolean isWalking;

    public Player(World world, float x, float y) {
        super(new Texture("players/player1.png"));
        this.world = world;
        setPosition(x - getWidth() / 2f, y - getHeight() / 2f);
        createBody();
        palyerAtlas = new TextureAtlas("animationPlayer/AnimationPlayer.atlas");
        setWalking(false);
    }

    void createBody() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        bodyDef.position.set(getX() / GameInfo.PPM, getY() / GameInfo.PPM);

        body = world.createBody(bodyDef);
        body.setFixedRotation(true);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((getWidth() / 10) / GameInfo.PPM, (getHeight() / 20) / GameInfo.PPM); //size of the box
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;

        Fixture fixture = body.createFixture(fixtureDef);

        shape.dispose();
    }

    public void movePlayerX(float x) {
        setWalking(true);
        //this.getBody().applyLinearImpulse(new Vector2(vel*x, 0), this.getBody().getWorldCenter(), true);
        body.setLinearVelocity(x, body.getLinearVelocity().y);
    }

    public void movePlayerY(float y) {
        setWalking(true);
        //this.getBody().applyLinearImpulse(new Vector2(0,vel*y), this.getBody().getWorldCenter(), true);
        body.setLinearVelocity(body.getLinearVelocity().x, y);
    }

    public void pausePlayerY() {
        setWalking(false);
        body.setLinearVelocity(0, 0);
    }

    public void updatePlayer() {
        this.setPosition(body.getPosition().x * GameInfo.PPM, body.getPosition().y * GameInfo.PPM);
    }

    public void drawPlayerIdle(SpriteBatch batch){
        if(!isWalking){
            batch.draw(this, getX()-70f, getY()-120f);
        }
    }

    public void drawPlayerAnimetion(SpriteBatch batch){
        if(isWalking) {
            elapsedTime += Gdx.graphics.getDeltaTime();
            Animation animation = new Animation(5f / 10, palyerAtlas.getRegions());

            Array<TextureAtlas.AtlasRegion> frames = palyerAtlas.getRegions();

            for (TextureRegion frame : frames) {
                if (body.getLinearVelocity().x < 0 && !frame.isFlipX()) {
                    frame.flip(true, false);
                } else if ((body.getLinearVelocity().x > 0 && frame.isFlipX())) {
                    frame.flip(true, false);
                } else if (body.getLinearVelocity().x == 0) {

                }
            }
            batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), getX() - 70, getY() + getHeight()- 280f);
        }
    }

    public void setWalking(boolean isWalking){
        this.isWalking = isWalking;
    }
}