package com.granados_raul_samso_joan.petitmon_v1.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Play the animation cow in the prize Screen
 */

public class Cow extends Sprite {

    private World world;
    private Body body;

    private TextureAtlas playerAtlas;
    private Animation animation;
    private float elapsedTime;


    public Cow(World world, String name, float x, float y) {
        super(new Texture(name));
        this.world = world;
        setPosition(x - getWidth() / 2f, y - getHeight() / 2f);
        createBody();
        playerAtlas = new TextureAtlas("animationCow/cow_rotation2.atlas");
    }

    void createBody() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(getX() / GameInfo.PPM, getY() / GameInfo.PPM);

        body = world.createBody(bodyDef);
        body.setFixedRotation(true);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((getWidth() / 10) / GameInfo.PPM, (getHeight() / 20) / GameInfo.PPM); //size of the box
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;

        Fixture fixture = body.createFixture(fixtureDef);

        shape.dispose();
    }

    public void drawPlayerAnimation(SpriteBatch batch) {

        elapsedTime += Gdx.graphics.getDeltaTime();

        Array<TextureAtlas.AtlasRegion> frames = playerAtlas.getRegions();

        for(TextureRegion frame : frames) {
            if(body.getLinearVelocity().x < 0 && !frame.isFlipX()) {
                frame.flip(true, false);
            } else if(body.getLinearVelocity().x > 0 && frame.isFlipX()) {
                frame.flip(true, false);
            }
        }

        animation = new Animation(1f/10f, playerAtlas.getRegions());

        batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), getX() + getWidth() / 2f - 20f,getY() - getHeight() / 2f);
    }

}