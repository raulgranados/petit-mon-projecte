package com.granados_raul_samso_joan.petitmon_v1.main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Timer;
import com.granados_raul_samso_joan.petitmon_v1.managers.AssetsManager;
import com.granados_raul_samso_joan.petitmon_v1.managers.ScreenManager;
import com.granados_raul_samso_joan.petitmon_v1.screens.ScreenEnum;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Initialize the application
 */

//public class GameMain extends ApplicationAdapter {
public class GameMain extends Game {

	private SpriteBatch batch;
	private static long SPLASH_MINIMUM_MILLIS = 3000L;

	private AssetsManager assets;

	public GameMain() {
		super();
	}

	@Override
	public void create () {

		batch = new SpriteBatch();

		ScreenManager.getInstance().initialize(GameMain.this);

		ScreenManager.getInstance().showScreen(ScreenEnum.SPLASH_SCREEN, GameMain.this);

		final long splash_start_time = System.currentTimeMillis();

		new Thread(new Runnable() {
			@Override
			public void run() {

				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {

						assets = new AssetsManager();
						assets.cargarAssets();

						long splash_elapsed_time = System.currentTimeMillis() - splash_start_time;

						if (splash_elapsed_time < GameMain.SPLASH_MINIMUM_MILLIS) {
							Timer.schedule(
									new Timer.Task() {
										@Override
										public void run() {
											ScreenManager.getInstance().initialize(GameMain.this);
											ScreenManager.getInstance().showScreen( ScreenEnum.LANGUAGE, GameMain.this );
											//ScreenManager.getInstance().showScreen( ScreenEnum.MAIN_MENU )
											//ScreenManager.getInstance().showScreen( ScreenEnum.INSTRUCTIONS, GameMain.this );;
											//ScreenManager.getInstance().showScreen( ScreenEnum.GAME_1,GameMain.this);
											//ScreenManager.getInstance().showScreen( ScreenEnum.GAME_1_PRICE, GameMain.this );
											//ScreenManager.getInstance().showScreen( ScreenEnum.GAME_2_SCORE, GameMain.this );
										}
									}, (float)(GameMain.SPLASH_MINIMUM_MILLIS - splash_elapsed_time) / 1000f);
						} else {
							ScreenManager.getInstance().initialize(GameMain.this);
							ScreenManager.getInstance().showScreen( ScreenEnum.LANGUAGE, GameMain.this );
							//ScreenManager.getInstance().showScreen( ScreenEnum.MAIN_MENU );
							//ScreenManager.getInstance().showScreen( ScreenEnum.INSTRUCTIONS, GameMain.this );
							//ScreenManager.getInstance().showScreen( ScreenEnum.GAME_1, GameMain.this);
							//ScreenManager.getInstance().showScreen( ScreenEnum.GAME_1_PRICE, GameMain.this );
							//ScreenManager.getInstance().showScreen( ScreenEnum.GAME_2_SCORE, GameMain.this );
						}
					}
				});
			}
		}).start();

	}

	@Override
	public void dispose () {
		super.dispose();
		getScreen().dispose();
		Gdx.app.exit();
	}

	public SpriteBatch getBatch() {
		return batch;
	}


}
