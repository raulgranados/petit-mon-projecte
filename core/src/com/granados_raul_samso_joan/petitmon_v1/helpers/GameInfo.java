package com.granados_raul_samso_joan.petitmon_v1.helpers;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Establish all the variables of the application
 */

public class GameInfo {

    public static final float WIDTH = 480.0f;
    public static final float HEIGHT = 800.0f;

    public static final int PPM = 100; // 100 pixels = 1 m

    public static boolean newGame1 = true;
    public static boolean newGame2 = true;

    //Detect when gets the scores from the ScoresMenu (then, varScoresMenu true)
    public static boolean varScoresMenu = false;

    public static final float[] PLAYERVELOCITIES  = {1.6f,2.0f,2.4f};

    //Languages
    public static int langId = 0; //Load the array element langRef[langId]
    public static final String[] langRef = {"es", "ca"}; //Element associate to language

    //GameImages (files names in the assets directory)
    public static final String[] buttonsRef = {
            "flags/flag_", //0
            "buttons/game_", //1
            "buttons/settings_", //2
            "buttons/game1_", //3
            "buttons/game2_", //4
            "buttons/soundYes_", //5
            "buttons/soundNot_", //6
            "buttons/level_", //7
            "buttons/back", //8
            "buttons/easy_", //9
            "buttons/medium_", //10
            "buttons/difficult_", //11
            "buttons/score_", //12
            "buttons/instructions_", //13
            "buttons/musicYes_", //14
            "buttons/musicNot_", //15
            "buttons/prize_"}; //16

    //Fonts
    public static BitmapFont font1,font2,font3;

    //Strings
    public static final String[][] messageLang={
            {"Puntos:",
                    "Vidas.",
                    "Tiempo:",
                    "Nivel",
                    "Objetivo:\n\n\ttocar la carta\ndiscordante",
                    "segundos",
                    "Objetivo:\n\n\temparejar cartas",
                    "Juego",
                    "Marcador\n\nJuego ",
                    "Partida",
                    "Top ",
                    "Instrucciones",
                    "Ver marcadores"},
            {"Punts:",
                    "Vides.",
                    "Temps:",
                    "Nivell",
                    "Objetiu:\n\n\ttocar la carta\ndiscordant",
                    "segons",
                    "Objetiu:\n\n\temparellar cartes",
                    "Joc",
                    "Marcador\n\nJoc ",
                    "Partida",
                    "Top ",
                    "Instruccions",
                    "Veure marcadors"}};

    //Sounds (Soundfiles names in the assets directory) -> Web page with free sounds -> https://www.freesound.org
    public static boolean soundsActivate = true;

    //ocells >> birds3
    public static final String[] soundsRef = {
            "cow",
            "eagle",
            "birds1",
            "birds2",
            "horse",
            "goat",
            "eagle",
            "birds3",
            "raven",
            "error",
            "trombone", //Sad Trombone Sound
            "bell"}; //Church Bell Chime Sound

    //Cards
    public static final int nCards = 3; //Number of cards
    public static final int nGrups = GameUtils.getNumOfGroups(); //Number of gropus
    public static ArrayList<String> cRefListGame1 = new ArrayList<String>(Arrays.asList(new String[4])); //Load the cards in the Game1
    public static ArrayList<String> cRefListGame2 =new ArrayList<String>(Arrays.asList(new String[6])); //Load the cards in the Game2

    //Music
    public static boolean musicActivate = true;
    public static final String[] musicRef= {"forest","nature"}; //http://soundbible.com/tags-forest.html (rainforest Ambience, nature Ambience)

    //Scores
    public static int currentScore1 =0;
    public static int currentScore2 =0;
    public static int plusScore=100;
    public static String[] arrayBestScores;
    public static ArrayList<String> arrayListbestScores;
    public static ArrayList<Integer> arrayListIntegerbestScores;

    //Lifes
    public static int currentLife;
    public static final int[] levelLivesGame1 ={5,4,3};
    public static final int[] levelLivesGame2 ={7,6,5};

    //Levels
    public static String[][] typeLevel={{"facil","medio","dificil"},{"facil","mig","dificil"}};
    public static int levelGame =1; //GameLevel (1 the easiest to 3 the most difficult)
    public static final int[] secondsLevelGame1 ={9,7,5}; //(index: 0 the easiest to 2 the most difficult)
    public static final int[] secondsLevelGame2 ={20,15,10}; //(index: 0 the easiest to 2 the most difficult)

}