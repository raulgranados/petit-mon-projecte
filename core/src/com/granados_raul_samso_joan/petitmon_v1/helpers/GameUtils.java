package com.granados_raul_samso_joan.petitmon_v1.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;

/**
 * Created by Raul Granados <raulgranados752@gmail.com> & Joan Samso <samsojoan1@gmail.com>
 *
 * @description Establish the general static methods of the application
 */

public class GameUtils {

    public static Music sounds, musics;

    public static void playSound(String string) {
        if (GameInfo.soundsActivate) {
            GameUtils.sounds = Gdx.audio.newMusic(Gdx.files.internal("sounds/".concat(string).concat(".mp3")));
            GameUtils.sounds.setVolume(10.0f);
            GameUtils.sounds.play();
        }
    }

    public static void playMusic(String string) {
        if (GameInfo.musicActivate) {
            GameUtils.musics = Gdx.audio.newMusic(Gdx.files.internal("sounds/".concat(string).concat(".mp3")));
            GameUtils.musics.setVolume(1.0f);
            GameUtils.musics.setLooping(true);
            GameUtils.musics.play();
        }
    }

    public static void stopMusic() {
        if (GameInfo.musicActivate) {
            GameUtils.musics.stop();
        }
    }

    //Create Random number between min and max, both included
    public static int getRandom(int min, int max) {

        return (int) (Math.random()*(max-min));
    }

    public static int getNumOfGroups (){
        FileHandle[] files = Gdx.files.internal("cards/").list();
        return (files.length-2)/3;
    }

    //Get a 2 digits-number ("1" to "01")
    public static String getSufix(int num) {
        String string=String.valueOf(num);
        if(string.length()==1) {
            string="0".concat(string);
        }
        return string;
    }
}
