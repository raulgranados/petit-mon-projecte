package com.granados_raul_samso_joan.petitmon_v1.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.granados_raul_samso_joan.petitmon_v1.helpers.GameInfo;
import com.granados_raul_samso_joan.petitmon_v1.main.GameMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = (int) GameInfo.WIDTH;
		config.height = (int) GameInfo.HEIGHT;
		config.forceExit = true;


		new LwjglApplication(new GameMain(), config);
	}
}
